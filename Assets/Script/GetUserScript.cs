﻿using System.Collections;
using UnityEngine;
using SimpleJSON;
using TMPro;

public class GetUserScript : MonoBehaviour
{

    public string linkApi;
    public TMP_Text textbox;

    void Start()
    {
        GetRequest();
    }

    void GetRequest()
    {
        WWW jsonReq = new WWW(linkApi);
        StartCoroutine(ProcessRequest(jsonReq));
    }

    private IEnumerator ProcessRequest(WWW req)
    {
        textbox.text = string.Empty;

        yield return req;

        textbox.text = "Loading...";

        JSONArray dataArr = JSON.Parse(req.text).AsArray;

        Debug.Log(dataArr);

        textbox.text =
                $"Name : {dataArr[5]["name"]}\n\n" +
                $"Email : {dataArr[5]["email"]}\n\n";
    }
}

